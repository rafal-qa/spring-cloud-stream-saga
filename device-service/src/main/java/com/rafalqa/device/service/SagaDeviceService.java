package com.rafalqa.device.service;

import com.rafalqa.device.common.Event;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import static com.rafalqa.device.common.Event.Service.METRICS;
import static com.rafalqa.device.common.Event.Status.CONFIRMED;
import static com.rafalqa.device.common.Event.Status.REJECTED;

@Service
@RequiredArgsConstructor
@Slf4j
public class SagaDeviceService {

  private final SagaTransactionService sagaTransactionService;
  private final MessageUtils messageUtils;

  public void processMessage(Message<Event> message) {
    Long dataId = (Long) message.getHeaders().get(KafkaHeaders.RECEIVED_MESSAGE_KEY);
    Event event = message.getPayload();

    log.info("DataId({}): {}", dataId, event);

    if (isMetricsConfirmedEvent(event)) {
      try {
        sagaTransactionService.commit(dataId, event);
      } catch (Exception e) {
        log.error(e.toString());
        messageUtils.sendToKafka(dataId, messageUtils.createDeviceEvent(event, REJECTED));
      }
    }
  }

  private boolean isMetricsConfirmedEvent(Event event) {
    return event.getService() == METRICS && event.getStatus() == CONFIRMED;
  }
}
