package com.rafalqa.device.service;

import com.rafalqa.device.common.Event;
import com.rafalqa.device.common.TestException;
import com.rafalqa.device.repository.DeviceEntity;
import com.rafalqa.device.repository.DeviceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

import static com.rafalqa.device.common.Event.Status.CONFIRMED;

@Service
@RequiredArgsConstructor
public class SagaTransactionService {

  private final DeviceRepository deviceRepository;
  private final MessageUtils messageUtils;

  @Transactional
  public void commit(Long dataId, Event event) {
    DeviceEntity device = deviceRepository.findById(event.getDeviceId()).orElseThrow();
    device.setLastActive(event.getDataTimestamp());

    if (event.getDataValue() == 40) throw new TestException(40);

    messageUtils.sendToKafka(dataId, messageUtils.createDeviceEvent(event, CONFIRMED));

    if (event.getDataValue() == 41) {
      sleep();
      throw new TestException(41);
    }
  }

  private void sleep() {
    try {
      TimeUnit.SECONDS.sleep(1);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}
