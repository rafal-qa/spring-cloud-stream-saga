package com.rafalqa.device;

import com.rafalqa.device.repository.DeviceEntity;
import com.rafalqa.device.repository.DeviceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
public class SampleData {

  private final DeviceRepository deviceRepository;

  @PostConstruct
  private void setupDevices() {
    List<DeviceEntity> devices =
        IntStream.rangeClosed(1, 10)
            .mapToObj(i -> DeviceEntity.builder().id(i).build())
            .collect(Collectors.toList());

    deviceRepository.saveAll(devices);
  }
}
