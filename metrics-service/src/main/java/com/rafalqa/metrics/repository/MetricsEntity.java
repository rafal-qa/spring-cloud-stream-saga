package com.rafalqa.metrics.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "metrics")
public class MetricsEntity {

  @Id private Integer deviceId;
  private Integer dataCount;
  private Long dataSum;
}
