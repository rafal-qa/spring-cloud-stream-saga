package com.rafalqa.metrics.common;

public class TestException extends RuntimeException {
  public TestException(Integer dataValue) {
    super("Test error for value = " + dataValue);
  }
}
