package com.rafalqa.metrics;

import com.rafalqa.metrics.common.Event;
import com.rafalqa.metrics.service.SagaMetricsService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.function.Consumer;
import java.util.function.Supplier;

@SpringBootApplication
public class MetricsServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(MetricsServiceApplication.class, args);
  }

  @Bean
  public Sinks.Many<Message<Event>> sink() {
    return Sinks.many().multicast().directBestEffort();
  }

  @Bean
  public Supplier<Flux<Message<Event>>> producer(Sinks.Many<Message<Event>> sink) {
    return sink::asFlux;
  }

  @Bean
  public Consumer<Message<Event>> consumer(SagaMetricsService sagaMetricsService) {
    return sagaMetricsService::processMessage;
  }
}
