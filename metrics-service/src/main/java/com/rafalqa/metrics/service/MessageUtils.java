package com.rafalqa.metrics.service;

import com.rafalqa.metrics.common.Event;
import com.rafalqa.metrics.common.TestException;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Sinks;

import static com.rafalqa.metrics.common.Event.Service.METRICS;

@Component
@RequiredArgsConstructor
public class MessageUtils {

  private final Sinks.Many<Message<Event>> sink;

  public void sendToKafka(Long dataId, Event event) {
    if (event.getDataValue() == 31) throw new TestException(31);

    Message<Event> message =
        MessageBuilder.withPayload(event).setHeader(KafkaHeaders.MESSAGE_KEY, dataId).build();

    sink.emitNext(message, Sinks.EmitFailureHandler.FAIL_FAST);
  }

  public Event createMetricsEvent(Event event, Event.Status status) {
    return Event.builder()
        .service(METRICS)
        .status(status)
        .deviceId(event.getDeviceId())
        .dataValue(event.getDataValue())
        .dataTimestamp(event.getDataTimestamp())
        .build();
  }
}
