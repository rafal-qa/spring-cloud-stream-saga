package com.rafalqa.metrics.service;

import com.rafalqa.metrics.common.Event;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import static com.rafalqa.metrics.common.Event.Service.DATA;
import static com.rafalqa.metrics.common.Event.Service.DEVICE;
import static com.rafalqa.metrics.common.Event.Status.PENDING;
import static com.rafalqa.metrics.common.Event.Status.REJECTED;

@Service
@RequiredArgsConstructor
@Slf4j
public class SagaMetricsService {

  private final SagaTransactionService sagaTransactionService;
  private final MessageUtils messageUtils;

  public void processMessage(Message<Event> message) {
    Long dataId = (Long) message.getHeaders().get(KafkaHeaders.RECEIVED_MESSAGE_KEY);
    Event event = message.getPayload();

    log.info("DataId({}): {}", dataId, event);

    if (isDataPendingEvent(event)) {
      try {
        sagaTransactionService.commit(dataId, event);
      } catch (Exception e) {
        log.error(e.toString());
        messageUtils.sendToKafka(dataId, messageUtils.createMetricsEvent(event, REJECTED));
      }
    }

    if (isDeviceRejectedEvent(event)) {
      sagaTransactionService.rollback(event);
    }
  }

  private boolean isDataPendingEvent(Event event) {
    return event.getService() == DATA && event.getStatus() == PENDING;
  }

  private boolean isDeviceRejectedEvent(Event event) {
    return event.getService() == DEVICE && event.getStatus() == REJECTED;
  }
}
