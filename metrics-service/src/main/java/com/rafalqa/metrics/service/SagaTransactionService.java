package com.rafalqa.metrics.service;

import com.rafalqa.metrics.common.Event;
import com.rafalqa.metrics.common.TestException;
import com.rafalqa.metrics.repository.MetricsEntity;
import com.rafalqa.metrics.repository.MetricsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.rafalqa.metrics.common.Event.Status.CONFIRMED;

@Service
@RequiredArgsConstructor
public class SagaTransactionService {

  private final MetricsRepository metricsRepository;
  private final MessageUtils messageUtils;

  @Transactional
  public void commit(Long dataId, Event event) {
    Integer deviceId = event.getDeviceId();
    Integer value = event.getDataValue();

    metricsRepository
        .findById(deviceId)
        .ifPresentOrElse(metrics -> update(metrics, value), () -> saveNew(deviceId, value));

    if (value == 30) throw new TestException(30);

    messageUtils.sendToKafka(dataId, messageUtils.createMetricsEvent(event, CONFIRMED));
  }

  private void update(MetricsEntity metrics, Integer value) {
    metrics.setDataCount(metrics.getDataCount() + 1);
    metrics.setDataSum(metrics.getDataSum() + value);
  }

  private void saveNew(Integer deviceId, Integer value) {
    metricsRepository.save(
        MetricsEntity.builder().deviceId(deviceId).dataCount(1).dataSum(value.longValue()).build());
  }

  @Transactional
  public void rollback(Event event) {
    if (event.getDataValue() == 32) throw new TestException(32);

    Integer deviceId = event.getDeviceId();
    Optional<MetricsEntity> optionalMetrics = metricsRepository.findById(deviceId);

    if (optionalMetrics.isEmpty()) return;

    MetricsEntity metrics = optionalMetrics.get();

    if (metrics.getDataCount() == 1) {
      metricsRepository.deleteById(deviceId);
      return;
    }

    metrics.setDataCount(metrics.getDataCount() - 1);
    metrics.setDataSum(metrics.getDataSum() - event.getDataValue());
  }
}
