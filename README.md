# Spring Cloud Stream Saga

Sample project demonstrating implementation of **Choreography Saga Pattern** using **Spring Cloud Stream** and **Kafka**. It also implements **Asynchronous Request-Reply Pattern** with a separate "status" endpoint.

Instead of solving real business problem, it focuses only on technical aspects, and is simplified as much as possible.

It proves that distributed transaction works and reveals edge cases when it doesn't.

## Problem it tries to solve

It tries to split local database transaction into distributed one.

Let's assume that we have some devices (e.g. air condition) and are collecting measured values (e.g. temperature).

We have 3 tables connected with foreign keys and any change is performed in a single transaction involving all tables:

* `device` - list of devices
* `data` - values related to devices
* `metrics` - aggregated data for every device (number of readings and sum for calculating average)

We want to move every table to a separate microservice with own database.

## Running

It requires **Java 17** and **Kafka** (I'm using Redpanda as a simpler Kafka replacement).

In every service's `application.yml` set proper `spring.cloud.stream.kafka.binder.brokers`.

Every service uses H2 in-memory database with available console:

* `data-service` http://localhost:8080/h2-console
* `metrics-service` http://localhost:8081/h2-console
* `device-service` http://localhost:8082/h2-console

### Endpoints

#### Add new (valid) data

```
curl -v -X POST -H "Content-Type: application/json" -d '{"deviceId":1,"dataValue":100}' http://localhost:8080/data
```

* Should return `202 Accepted` with `Location` header pointing to `/data/status/1`

#### Check status for (first) data entry

```
curl -v http://localhost:8080/data/status/1
```

* When `PENDING` Saga, should return `200 OK`
* Otherwise, should return `303 See Other` with `Location` header pointing to `/data/1`

#### Get (first) data entry

```
curl -v http://localhost:8080/data/1
```

* When `CONFIRMED` Saga, should return `200 OK` with data from database
* When `REJECTED` Saga, should return `400 Bad Request`
* When `PENDING` Saga, should return `404 Not Found`

## Architecture

Single Kafka topic (with single `Event`) is used for the whole architecture and was omitted on diagram (it's much simpler).

`Event` contains data required by every service and following metadata:

* `Service` - `DATA`, `METRICS`, `DEVICE`
* `Status` - `PENDING`, `CONFIRMED`, `REJECTED`

`data-service` is an entry point, propagates changes to other services and holds the final status of distributed transaction.

![diagram](doc/diagram.png)

## Testing

* `deviceId = 1-10`
    * Valid, added to `device-service` database on every startup
    * Saga will finish with `CONFIRMED`
* `deviceId > 10`
    * Invalid, `device-service` will return error
    * Saga will finish with `REJECTED`
    * Changes in `metrics-service` will be rolled back

### Special values

Some `dataValue` will return exceptions in different places

* `2x` in `data-service`
* `3x` in `metrics-service`
* `4x` in `device-service`

| Value | Place in code                                                           | Effect                                                                                                                                            | Correct?           |
|-------|-------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|--------------------|
| 20    | After saving `DataEntity`, but before sending a message                 | Local database transaction will be rolled back, Saga won't start                                                                                  | :white_check_mark: |
| 21    | During committing Saga in `DataEntity`                                  | Saga will stuck in `PENDING`, no rollback in other services                                                                                       | :x:                |
| 30    | After updating/creating `MetricsEntity`, but before sending a message   | No local updates, `REJECTED` message will be sent                                                                                                 | :white_check_mark: |
| 31    | During sending any message                                              | No local updates (`MetricsEntity` changes will be rolled back), no message will be sent, Saga will stuck in `PENDING`                             | :x:                |
| 32    | During rolling back changes in `MetricsEntity` as a part of Saga        | Changes will be persisted, but shouldn't. To simulate this error, provide invalid `deviceId`                                                      | :x:                |
| 40    | After updating `DeviceEntity`, but before sending confirmation message  | Local database transaction will be rolled back, `REJECTED` message will be sent                                                                   | :white_check_mark: |
| 41    | 1 second after updating `DeviceEntity` and sending confirmation message | Saga will be `CONFIRMED`, but later local database transaction will be rolled back causing sending `REJECTED` message and properly rejecting Saga | :x:                |
