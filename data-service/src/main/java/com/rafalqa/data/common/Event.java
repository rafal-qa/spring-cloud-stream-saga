package com.rafalqa.data.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Event {
  private Service service;
  private Status status;
  private Integer deviceId;
  private Integer dataValue;
  private LocalDateTime dataTimestamp;

  public enum Service {
    DATA,
    METRICS,
    DEVICE
  }

  public enum Status {
    PENDING,
    CONFIRMED,
    REJECTED
  }
}
