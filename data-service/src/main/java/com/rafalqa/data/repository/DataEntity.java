package com.rafalqa.data.repository;

import com.rafalqa.data.common.Event;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "data")
public class DataEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Integer deviceId;
  private Integer dataValue;
  private LocalDateTime dataTimestamp;

  @Enumerated(EnumType.STRING)
  private Event.Status status;
}
