package com.rafalqa.data.controller;

import com.rafalqa.data.common.Event;
import com.rafalqa.data.repository.DataEntity;
import com.rafalqa.data.service.SagaDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static com.rafalqa.data.common.Event.Status.*;

@RestController
@RequiredArgsConstructor
public class Controller {

  private final SagaDataService sagaDataService;

  @PostMapping("/data")
  public ResponseEntity<DataEntity> addData(@RequestBody AddDataRequest request) {
    DataEntity pendingData = sagaDataService.produceDataPending(request);

    return ResponseEntity.status(HttpStatus.ACCEPTED)
        .header(HttpHeaders.LOCATION, "/data/status/" + pendingData.getId())
        .body(pendingData);
  }

  @GetMapping("/data/status/{id}")
  public ResponseEntity<Event.Status> getStatus(@PathVariable Long id) {
    Event.Status status = sagaDataService.getData(id).getStatus();

    if (status == PENDING) {
      return ResponseEntity.ok().body(status);
    }

    return ResponseEntity.status(HttpStatus.SEE_OTHER)
        .header(HttpHeaders.LOCATION, "/data/" + id)
        .body(status);
  }

  @GetMapping("/data/{id}")
  public DataEntity getData(@PathVariable Long id) {
    DataEntity data = sagaDataService.getData(id);

    if (data.getStatus() == CONFIRMED) {
      return data;
    }

    if (data.getStatus() == REJECTED) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  }
}
