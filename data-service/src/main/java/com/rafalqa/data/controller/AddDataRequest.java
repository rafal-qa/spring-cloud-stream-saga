package com.rafalqa.data.controller;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddDataRequest {
  private Integer deviceId;
  private Integer dataValue;
}
