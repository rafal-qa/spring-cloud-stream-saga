package com.rafalqa.data.service;

import com.rafalqa.data.common.Event;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Sinks;

import static com.rafalqa.data.common.Event.Service.DATA;

@Component
@RequiredArgsConstructor
public class MessageUtils {

  private final Sinks.Many<Message<Event>> sink;

  public void sendToKafka(Long dataId, Event event) {
    Message<Event> message =
        MessageBuilder.withPayload(event).setHeader(KafkaHeaders.MESSAGE_KEY, dataId).build();

    sink.emitNext(message, Sinks.EmitFailureHandler.FAIL_FAST);
  }

  public Event createDataEvent(Event event, Event.Status status) {
    return Event.builder()
        .service(DATA)
        .status(status)
        .deviceId(event.getDeviceId())
        .dataValue(event.getDataValue())
        .dataTimestamp(event.getDataTimestamp())
        .build();
  }
}
