package com.rafalqa.data.service;

import com.rafalqa.data.common.Event;
import com.rafalqa.data.common.TestException;
import com.rafalqa.data.repository.DataEntity;
import com.rafalqa.data.repository.DataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.rafalqa.data.common.Event.Status.CONFIRMED;
import static com.rafalqa.data.common.Event.Status.REJECTED;

@Service
@RequiredArgsConstructor
public class SagaTransactionService {

  private final DataRepository dataRepository;
  private final MessageUtils messageUtils;

  @Transactional
  public void commit(Long dataId, Event event) {
    DataEntity data = dataRepository.findById(dataId).orElseThrow();
    data.setStatus(CONFIRMED);

    if (event.getDataValue() == 21) throw new TestException(21);

    messageUtils.sendToKafka(dataId, messageUtils.createDataEvent(event, CONFIRMED));
  }

  @Transactional
  public void rollback(Long dataId, Event event) {
    DataEntity data = dataRepository.findById(dataId).orElseThrow();
    data.setStatus(REJECTED);

    messageUtils.sendToKafka(dataId, messageUtils.createDataEvent(event, REJECTED));
  }
}
