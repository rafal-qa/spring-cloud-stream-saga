package com.rafalqa.data.service;

import com.rafalqa.data.common.Event;
import com.rafalqa.data.common.TestException;
import com.rafalqa.data.controller.AddDataRequest;
import com.rafalqa.data.repository.DataEntity;
import com.rafalqa.data.repository.DataRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static com.rafalqa.data.common.Event.Service.*;
import static com.rafalqa.data.common.Event.Status.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class SagaDataService {

  private final SagaTransactionService sagaTransactionService;
  private final DataRepository dataRepository;
  private final MessageUtils messageUtils;

  public DataEntity getData(Long id) {
    return dataRepository.findById(id).orElseThrow();
  }

  @Transactional
  public DataEntity produceDataPending(AddDataRequest request) {
    DataEntity pendingData = savePending(request);

    if (request.getDataValue() == 20) throw new TestException(20);

    Event event =
        Event.builder()
            .service(DATA)
            .status(pendingData.getStatus())
            .deviceId(pendingData.getDeviceId())
            .dataValue(pendingData.getDataValue())
            .dataTimestamp(pendingData.getDataTimestamp())
            .build();

    messageUtils.sendToKafka(pendingData.getId(), event);

    return pendingData;
  }

  private DataEntity savePending(AddDataRequest request) {
    DataEntity pendingData =
        DataEntity.builder()
            .deviceId(request.getDeviceId())
            .dataValue(request.getDataValue())
            .dataTimestamp(LocalDateTime.now())
            .status(PENDING)
            .build();

    return dataRepository.save(pendingData);
  }

  public void processMessage(Message<Event> message) {
    Long dataId = (Long) message.getHeaders().get(KafkaHeaders.RECEIVED_MESSAGE_KEY);
    Event event = message.getPayload();

    log.info("DataId({}): {}", dataId, event);

    if (isDeviceConfirmedEvent(event)) {
      sagaTransactionService.commit(dataId, event);
    }

    if (isDeviceRejectedEvent(event) || isMetricsRejectedEvent(event)) {
      sagaTransactionService.rollback(dataId, event);
    }
  }

  private boolean isDeviceConfirmedEvent(Event event) {
    return event.getService() == DEVICE && event.getStatus() == CONFIRMED;
  }

  private boolean isDeviceRejectedEvent(Event event) {
    return event.getService() == DEVICE && event.getStatus() == REJECTED;
  }

  private boolean isMetricsRejectedEvent(Event event) {
    return event.getService() == METRICS && event.getStatus() == REJECTED;
  }
}
