package com.rafalqa.data;

import com.rafalqa.data.common.Event;
import com.rafalqa.data.service.SagaDataService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.function.Consumer;
import java.util.function.Supplier;

@SpringBootApplication
public class DataServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(DataServiceApplication.class, args);
  }

  @Bean
  public Sinks.Many<Message<Event>> sink() {
    return Sinks.many().multicast().directBestEffort();
  }

  @Bean
  public Supplier<Flux<Message<Event>>> producer(Sinks.Many<Message<Event>> sink) {
    return sink::asFlux;
  }

  @Bean
  public Consumer<Message<Event>> consumer(SagaDataService sagaDataService) {
    return sagaDataService::processMessage;
  }
}
